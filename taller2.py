import sqlalchemy as db

engine = db.create_engine('sqlite:///asign1ps.db')
conn = engine.connect()
metadata = db.MetaData()
persona = db.Table('diccionariops', metadata, autoload=True, autoload_with=engine)


class ModeloBase(Model):
    class Meta:
        database = conn


class Palabra(ModeloBase):
    PALABRA = TextField()
    SIGNIFICADO = TextField()


def crear_tablas():
    conn.connect()
    conn.create_tables([Palabra])


def principal():
    crear_tablas()
    MENU = """

a) Agregar una nueva Palabra.
b) Editar alguna Palabra existente.
c) Eliminar alguna Palabra existente.
d) Ver listado de Palabras.
e) Buscar significado de X Palabra.
f) Salir.

Elige: """

    elec = ""
    while elec != "f":
        elec = input(MENU)

        if elec == "a":
            palabra = input("Ingrese la Palabra porfavor: ")

            posible_significado = buscar_significado_palabra(PALABRA)

            if posible_significado:
                print(f"La Palabra '{PALABRA}' ya existe!")
            else:
                SIGNIFICADO = input("Ingrese el Significado porfavor: ")
                agregarp(PALABRA, SIGNIFICADO)
                print("La Palabra ha sido agregada satisfactoriamente!!")

        if elec == "b":
            PALABRA = input("Ingrese la Palabra que desea editar: ")
            nuevosign = input("Ingrese el nuevo significado de la misma: ")
            editarp(PALABRA, nuevosign)
            print("La Palabra ha sido actualizada correctamente!!")

        if elec == "c":
            PALABRA = input("Ingrese la Palabra que desea eliminar porfavor: ")
            eliminarp(PALABRA)

        if elec == "d":
            palabras = obtenerp()
            print("=== Lista de palabras ===")
            for PALABRA in palabras:
                print(PALABRA[0])

        if elec == "e":
            PALABRA = input(
                "Ingrese la Palabra de la cual desea saber el significado porfavor: ")
            SIGNIFICADO = buscar_significado_palabra(PALABRA)
            if SIGNIFICADO:
                print(
                    f"El Significado de '{PALABRA}' es:\n{SIGNIFICADO}")
            else:
                print(f"La Palabra '{PALABRA}' no fue encontrada")


def agregarp(PALABRA, SIGNIFICADO):
    Palabra.create(palabra=PALABRA, significado=SIGNIFICADO)


def editarp(PALABRA, nuevosign):
    Palabra.update({Palabra.significado: nuevosign}).where(
        Palabra.palabra == palabra).execute()


def eliminarp(PALABRA):
    Palabra.delete().where(Palabra.palabra == palabra).execute()


def obtenerp():
    return Palabra.select()


def buscar_significado_palabra(PALABRA):
    
    try:
        return Palabra.select().where(Palabra.palabra == palabra).get().significado
    except Exception:

if __name__ == '__main__':
    principal()


conn.commit()

conn.close()


